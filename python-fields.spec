%global pypi_name fields

Name:           python-%{pypi_name}
Version:        5.0.0
Release:        1
Summary:        Container class boilerplate killer

License:        BSD-2-Clause
URL:            https://github.com/ionelmc/python-fields
Source0:        https://files.pythonhosted.org/packages/18/68/b922b5b0b2c1d99171c0ed9ad10296b55ee644eb1fa2fd5e45cafe56ae33/fields-5.0.0.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:	python3-hatchling
BuildRequires:	python3-pdm-pep517
BuildRequires:	python3-pdm-backend
BuildRequires:	python3-pytest

%description
Container class boilerplate killer.

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name}
Container class boilerplate killer.

%package -n python-%{pypi_name}-doc
Summary:        fields documentation
%description -n python-%{pypi_name}-doc
Documentation for fields

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%pyproject_build


%install
%pyproject_install


%files -n python3-%{pypi_name}
%license LICENSE
%doc README.rst docs/readme.rst
%{python3_sitelib}/%{pypi_name}*



%changelog
* Thu Sep 14 2023 luolu12 <luluoc@isoftstone.com> - 5.0.0-1
- Initial package.
